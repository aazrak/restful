<?php namespace Aazrak\Restful\Classes;

use Exception;
use Illuminate\Http\Request;
use October\Rain\Foundation\Exception\Handler;
use Event;
use Response;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

// As an example exception you want to handle ...

/* Custom error handler which replaces the default error handler for OctoberCMS. */

class APIErrorHandler extends Handler
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        //dd($exception);
        /* Custom JSON response for NotFoundHttpException exceptions. */
        if ($exception instanceof NotFoundHttpException && $request->wantsJson()) {
            return response()->error('resource_not_found', [], 404);
        }

        /* The rest of this code is just the 'default' code from OctoberCMS' error handler. */
        return parent::render($request, $exception);
    }
}
