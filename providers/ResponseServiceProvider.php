<?php namespace Aazrak\Restful\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\ResponseFactory;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param ResponseFactory $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('success', function ($message = '', $data = null, $code = 200) use ($factory) {
            $format = [
                'status' => 'ok',
                'message' => $message,
                'result' => $data,
            ];

            return $factory->json($format, $code);
        });

        $factory->macro('error', function (string $message = '', $errors = [], $code = 500) use ($factory) {
            $format = [
                'status' => 'error',
                'message' => $message,
                'result' => ["errors" => $errors],
            ];

            return $factory->json($format, $code);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
