<?php namespace Aazrak\Restful\Providers;

use Config;

class APIAuthServiceProvider extends \Tymon\JWTAuth\Providers\JWTAuthServiceProvider
{

    /**
     * Helper to get the config values.
     *
     * @param string $key
     * @return string
     */
    protected function config($key, $default = null)
    {
        $val = Config::get('aazrak.restful::' . $key);
        return $val ?: config("jwt.$key", $default);
    }
}
