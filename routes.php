<?php

use Aazrak\Restful\Models\Settings;
use RainLab\User\Models\User as UserModel;

Route::group(['prefix' => Config::get('aazrak.api.prefix', 'api/v1')], function () {

    Route::post(Config::get('aazrak.api.routes.login', 'login'), function (Request $request) {
        $login_fields = Settings::get('login_fields', ['email', 'password']);
        $credentials = Input::only($login_fields);

        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->error('invalid_credentials', ['The email or password are wrong.'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->error('could_not_create_token', [$e->getMessage()], 500);
        }
        // if no errors are encountered we can return a JWT
        return response()->success('success', $token);
    });

    Route::post(Config::get('aazrak.api.routes.register', 'register'), function (Request $request) {

        $register_fields = Settings::get('register_fields', (new UserModel())->getFillable());
        $credentials = Input::only($register_fields);
        $credentials['created_ip_address'] = Request::getClientIp();
        try {
            $user = UserModel::create($credentials);
            $user->reload();
        } catch (Exception $e) {
            return response()->error('failed', [$e->getMessage()], 401);
        }

        $token = JWTAuth::fromUser($user);

        return response()->success('success', compact('token', 'user'));
    });

    Route::post(Config::get('aazrak.api.routes.invalidate', 'invalidate'), function (Request $request) {

        $token = Request::get('token');

        try {
            // invalidate the token
            JWTAuth::invalidate($token);
        } catch (Exception $e) {
            // something went wrong
            return response()->error('could_not_invalidate_token', [$e->getMessage()], 500);
        }

        // if no errors we can return a message to indicate that the token was invalidated
        return response()->success('success','token_invalidated');
    });

    Route::get(Config::get('aazrak.api.routes.show', 'user'), function (Request $request) {
        $user = JWTAuth::toUser();
        return response()->success('success', $user);
    })->middleware('jwt.auth');

    Route::put(Config::get('aazrak.api.routes.update', 'user'), function (Request $request) {
        $invalidInputs = array_merge(Settings::get('update_fields_except', [])
            , ['is_superuser', 'reset_password_code', 'activation_code', 'persist_code', 'role_id']);
        $input = Input::except($invalidInputs);
        $user = JWTAuth::toUser();
        //$user = UserModel::findByEmail($user->email);
        if (!$user->update($input)) {
            return response()->error('failed', ['Unable to update user']);
        }
        return response()->success('success', $user);
    })->middleware('jwt.auth');

});
