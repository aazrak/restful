<?php namespace Aazrak\Restful;

use Aazrak\Restful\Classes\APIErrorHandler;
use App;
use Backend;
use Config;
use Event;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\AliasLoader;
use RainLab\User\Models\User;
use Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use System\Classes\PluginBase;

/**
 * Restful Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array   Require the RainLab.User plugin
     */
    public $require = ['RainLab.User'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'Restful',
            'description' => 'No description provided yet...',
            'author' => 'Aazrak',
            'icon' => 'icon-leaf'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return void
     */
    public function boot()
    {
        /* Replace the default error handler of OctoberCMS to return JSON format responses instead. */
        /* Also, this is used in order to control the responses for certain types of errors/exceptions. */
        $this->app->bind(
            ExceptionHandler::class,
            APIErrorHandler::class
        );
        $this->app->bind(\Illuminate\Auth\AuthManager::class, function ($app) {
            return new \Illuminate\Auth\AuthManager($app);
        });
        $configKeys = ['defaults', 'guards', 'providers', 'passwords'];
        foreach ($configKeys as $key) {
            if (empty(Config::get('auth.' . $key))) {
                Config::set('auth.' . $key, Config::get('aazrak.restful::auth.' . $key));
            }
        }


        \App::register(\Aazrak\Restful\Providers\APIAuthServiceProvider::class);
        \App::register(\Aazrak\Restful\Providers\ResponseServiceProvider::class);

        $facade = AliasLoader::getInstance();
        $facade->alias('JWTAuth', '\Tymon\JWTAuth\Facades\JWTAuth');
        $facade->alias('JWTFactory', '\Tymon\JWTAuth\Facades\JWTFactory');


        \App::singleton('auth', function ($app) {
            return new \Illuminate\Auth\AuthManager($app);
        });

        $this->app->alias(\Aazrak\Restful\Http\JWTAuthMiddleware::class, 'jwt.auth');
        $this->app['router']->middleware('jwt.refresh', '\Tymon\JWTAuth\Middleware\RefreshToken');

        Event::listen('tymon.jwt.valid', function (User $user){
            $user->touchLastSeen();
            if($ip = Request::getClientIp()){
                $user->touchIpAddress($ip);
            }
        });
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'aazrak.restful.access_settings' => [
                'tab' => 'Restful',
                'label' => 'Access API Routes Settings'
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'REST API Settings',
                'description' => 'Manage prefix & auth routes.',
                'category' => 'REST API',
                'icon' => 'icon-leaf',
                'class' => 'Aazrak\Restful\Models\Settings',
                'order' => 500,
                'keywords' => 'security rest api',
                'permissions' => ['aazrak.restful.access_settings']
            ]
        ];
    }

}
