<?php namespace Aazrak\Restful\Models;

use Config;
use Model;

/**
 * Model
 *
 * @property int $id
 * @property string|null $item
 * @property string|null $value
 * @method static \October\Rain\Database\Builder|\Aazrak\Restful\Models\Settings newModelQuery()
 * @method static \October\Rain\Database\Builder|\Aazrak\Restful\Models\Settings newQuery()
 * @method static \October\Rain\Database\Builder|\Aazrak\Restful\Models\Settings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Aazrak\Restful\Models\Settings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Aazrak\Restful\Models\Settings whereItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Aazrak\Restful\Models\Settings whereValue($value)
 * @mixin \Eloquent
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'aazrak_restful_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
