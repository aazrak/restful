<?php


namespace Aazrak\Restful\Http;


use RainLab\User\Models\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class JWTAuthMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (!$token = $this->auth->setRequest($request)->getToken()) {
            $this->events->fire('tymon.jwt.absent');
            return $this->response->error('token_not_provided', ["The authentication token was not provided. Please use the /login endpoint to obtain one, or register a new user"], 400);
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            $this->events->fire('tymon.jwt.expired', [$e]);
            return $this->response->error('token_expired', [$e->getMessage()], $e->getStatusCode());
        } catch (JWTException $e) {
            $this->events->fire('tymon.jwt.invalid', [$e]);
            return $this->response->error('token_invalid', [$e->getMessage()], $e->getStatusCode());
        }

        if (!$user) {
            $this->events->fire('tymon.jwt.user_not_found');
            return $this->response->error('user_not_found', ["The email or password are wrong"], 404);
        }
        $this->events->fire('tymon.jwt.valid', $user);
        return $next($request);
    }
}
