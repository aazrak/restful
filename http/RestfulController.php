<?php namespace Aazrak\Restful\Http;

use App;
use Backend\Classes\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Model;
use October\Rain\Database\QueryBuilder;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Generic API Controller
 */
class RestfulController extends Controller
{
    /**
     * @var QueryBuilder an instance of the model that will be used to query upon
     */
    protected $model;

    protected $availableActions = ['index', 'show', 'create', 'update', 'destroy'];

    /**
     * @var array define non accessible actions
     */
    protected $disabledActions = [];

    /**
     * RestfulController constructor.
     * @param Model $model an instance of the model that will be used to query upon
     * @param array $authenticatedActions an array of actions that only accessible for authenticated users
     */
    public function __construct($model, array $authenticatedActions = [])
    {
        $this->model = $model;
        if (count($authenticatedActions) > 0)
            $this->middleware('jwt.auth')->except(array_diff($this->availableActions, $authenticatedActions));
        parent::__construct();
    }

    /**
     * Returns the Database model.
     * @return QueryBuilder
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Returns the current authenticated user
     * @return mixed
     */
    public function getUser()
    {
        return JWTAuth::toUser();
    }

    /**
     * Returns a model with the applied filters
     * @return QueryBuilder
     */
    public function getScopedModel()
    {
        $scopes = $this->getScopes();
        $filtered = $this->getModel();
        if ($scopes && count($scopes) > 0) {
            foreach ($scopes as $scope) {
                if (method_exists($filtered, "scope" . studly_case($scope)))
                    $filtered = $filtered->$scope();
            }
        }
        return $filtered;
    }

    /**
     * Returns the scopes which will be applied on each query for the index
     * @return array
     */
    public function getScopes()
    {
        return [];
    }


    public function index($param1 = null)
    {
        if (array_has($this->disabledActions, 'index'))
            App::abort(404);
        $per_page = get('per_page');
        if ($per_page == 0) {
            $per_page = $this->getScopedModel()->count();
        }
        $data = $this->getScopedModel()->paginate($per_page);
        return response()->success(null, $data);
    }

    public function show($id)
    {
        if (array_has($this->disabledActions, 'show'))
            App::abort(404);
        $data = $this->getModel()->where('id', $id)->first();
        if ($data) {
            return response()->success(null, $data);
        } else {
            return response()->error('failed', ['No item was found with the provided id']);
        }
    }

    public function create($data)
    {
        if (array_has($this->disabledActions, 'create'))
            App::abort(404);
        $item = $this->getModel()::create($data);
        if ($item)
            return response()->sucess('Item created successfully.', $item);
        return response()->error('failed', ['Could not create a new item']);
    }

    public function update($id, $data)
    {
        if (array_has($this->disabledActions, 'update'))
            App::abort(404);
        $item = $this->getScopedModel()::where('id', $id)->first();
        if ($item) {
            $item->fill($data);
            $item->save();
        } else {
            return response()->error('failed', ['No item was not found to update. Try creating the item first']);
        }
    }

    public function destroy($id)
    {
        if (array_has($this->disabledActions, 'destroy'))
            App::abort(404);
        try {
            $item = $this->getModel()::where('id', $id)->firstOrFail();
            $item->delete();
            return response()->success('Item deleted successfully');
        } catch (ModelNotFoundException $ex) {
            return response()->error('failed', ['Item could not be deleted']);
        }
    }
}
